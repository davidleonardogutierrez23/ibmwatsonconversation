﻿using IBM.WatsonDeveloperCloud.Conversation.v1;
using IBM.WatsonDeveloperCloud.Conversation.v1.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ConversationApi.Controllers
{
    [Route("api/[controller]")]
    public class ConversationsController : Controller
    {
        ConversationService _conversation = new ConversationService();

        // GET: /<controller>/
        public string Index()
        {
            _conversation.SetCredential("a749da5b-b40f-455b-9de8-76e416007b63", "Cun6H67TR6cB");

            MessageRequest messageRequest = new MessageRequest()
            {
                Input = new InputData()
                {
                    Text = "Mi Gato esta enfermo"
                }
            };

            var result = _conversation.Message("f67e24ea-d02d-41ac-afdd-95457356b5c5", messageRequest);

            return result.Output.text[0];
        }
    }
}
